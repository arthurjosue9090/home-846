#ifndef MTA_CLIENT
#define MTA_CLIENT

#define CUSTOM_BUILD
#define CLIENT_VERSION "0.846.1"

#ifdef CUSTOM_BUILD
    #define CLIENT_AC_KEY "user-client"
#else
    #include "../ac_key.h"
#endif

#define MTASA_VERSION_MAJOR         1
#define MTASA_VERSION_MINOR         5
#define MTASA_VERSION_MAINTENANCE   8
#define MTASA_VERSION_BUILD         0

#define MTA_DM_FULL_STRING          "MTA:SA Client"

#endif // MTA_CLIENT
