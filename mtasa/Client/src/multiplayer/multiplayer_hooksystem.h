#ifndef MULTIPLAYER_HOOKSYSTEM_H
#define MULTIPLAYER_HOOKSYSTEM_H

void HookInstall(uintptr_t addr, uintptr_t func, uintptr_t *orig);
void PLTHookInstall(uintptr_t addr, uintptr_t func, uintptr_t *orig);
void MTDHookInstall(uintptr_t addr, uintptr_t func);

#define hookA(addr, func, orig) \
    HookInstall(addr, func, orig)

#define hookB(addr, func, orig) \
    PLTHookInstall(addr, func, orig)

#define hookC(addr, func, orig) \
    MTDHookInstall(addr, func)

#endif // MULTIPLAYER_HOOKSYSTEM_H