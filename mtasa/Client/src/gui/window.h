#ifndef WINDOW_H
#define WINDOW_H

#include "../../vendor/imgui/imgui.h"
#include "../../vendor/imgui/imgui_internal.h"

#include "keyboard.h"
#include "mainmenu.h"

enum eTouchType
{
	TOUCH_POP = 1,
	TOUCH_PUSH = 2,
	TOUCH_MOVE = 3
};

#define RGBA_TO_FLOAT(r,g,b,a) (float)r/255.0f, (float)g/255.0f, (float)b/255.0f, (float)a/255.0f
#define RGBA_ABGR(color) color = ((color << 24) | ((color << 8) & 0x00FF0000) | ((color >> 8) & 0x0000FF00) | (color >> 24))
#define ARGB_RGBA(color) color = (color << 8 | color >> 24)

class window
{
    friend class mainmenu;
private:
    float g_fPosX;
    float g_fPosY;
    float g_fSizeX;
    float g_fSizeY;
    ImColor g_bgColor, g_textColor;
    bool g_bShow;    
    uint32_t g_ID;
    bool g_bLockPad;
    float g_fFontSize;
    ImFont* g_pFont;
public:
    window(uint32_t, float, float, float, float, ImColor, ImColor, bool, bool);
    ~window();

    void draw();
    bool touch(float, float, uint8_t);
    
    bool checkIn(float, float);
    void setPosition(float, float);
    void setSize(float, float);
    void setBackgroundColor(ImColor);
    void setTextColor(ImColor);
    void setVisibility(bool);
    ImFont* loadFont(char *, float);
    void addText(float, ImFont*, ImVec2&, ImU32, bool, const char*, const char* text_end = nullptr);
    void fill(ImVec2&, ImVec2&, ImU32);
    void processPop(float, float);
    void processPush(float, float);
    void processMove(float, float);
    void setPadLocked(bool bLock);
    void drawButtons();
    ImFont* getFont() { return g_pFont; };
};

#endif // WINDOW_H
