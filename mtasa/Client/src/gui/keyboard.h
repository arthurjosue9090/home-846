#ifndef KEYBOARD_H
#define KEYBOARD_H

/* ========== DESTROY THIS SHIT AFTER ========== */

class keyboard
{
private:
    static char inputBuffer[512];
    static bool g_bDraw;
    static bool g_bShift;
    static uint g_iType;
    static bool g_bSpecialSym;
public:
    static void addChar(char);
    static void removeChar();
    static void setText(char*);
    static char* getText();
    static void draw();
    static void setVisibility(bool);
    static void saveHistory(const std::string& msg);
    static void scrollUpHistory();
    static void scrollDownHistory();
    static void allowHistory(bool s);
    static void pushShift();
};

#endif // KEYBOARD_H