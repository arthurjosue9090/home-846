#ifndef IMGUIWRAPPER_H
#define IMGUIWRAPPER_H

#include "window.h"

struct stRect
{
	int x1;	// left
	int y1;	// top
	int x2;	// right
	int y2;	// bottom
};

struct stfRect
{
	float x1;
	float y1;
	float x2;
	float y2;
};

class ImGuiWrapper
{
private:
    /* data */
public:
    static void RenderDrawData(ImDrawData* draw_data);
    static bool Init();
    static bool CreateDeviceObjects();
    static void ShutDown();
    static void NewFrame();
    static void DrawRaster(stRect *rect, uint32_t color, RwRaster *raster = nullptr, stfRect *uv = nullptr);
    static void CreateSTFRect(stRect* rect, stfRect* uv, float rc1, float rc2, float rc3, float rc4, float uv1, float uv2, float uv3, float uv4);
};

#endif // IMGUIWRAPPER_H