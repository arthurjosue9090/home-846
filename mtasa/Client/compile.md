**Build Instructions**

**[Windows]**

| Prerequisites | 
| ------ | 
| Visual Studio Code | 
| Android NDK for Windows 64-bit (x86) (optimal version is r22) | 

ATTENTION! 
If you chose a different version of the NDK from the recommended one, then edit the executable file 'build.bat' yourself.

1) Download the required NDK and place it in the '/ndk/' folder, next to your project. 

    It should turn out like this:

    .../your_project/jni/* (SOURCE)
    
    .../ndk/android-ndk-YOUR_VERSION/* (NDK)


2) Сreate the 'jni' folder (do as showed in the first step) and put the files from the 'src' folder in the archive into it.
3) Execute 'build.bat' in '/jni/' folder from terminal. (Steps: Terminal > New terminal > ./build.bat)

**[Linux]**

| Prerequisites | 
| ------ | 
| Visual Studio Code | 
| Android NDK for Linux 64-bit (x86) (optimal version is r21e) | 


ATTENTION! 
If you chose a different version of the NDK from the recommended one, then edit the executable file 'build-linux.cmd' yourself.


1) Download the required NDK and place it in the '/ndk/' folder, next to your project. 

    It should turn out like this:

    .../your_project/jni/* (SOURCE)
    
    .../ndk/android-ndk-YOUR_VERSION/* (NDK)


2) Сreate the 'jni' folder (do as showed in the first step) and put the files from the 'src' folder in the archive into it.
3) Execute 'build-linux.cmd' in '/jni/' folder from terminal. (Steps: Terminal > New terminal > ./build-linux.cmd)
